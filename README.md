# Screencast_Spotify-Big-Data

Dieses Repository enthält nur eine kurze Beschreibung für ein Uni-Projekt mit zusätzlich 3 Links (YouTube) zur Darstellung des Screencasts der Prüfungsleistung. Links sind in Youtube, da in Github keine Datensätze > 100Mb zulässig sind.

Dieses Repository ist ab dem 23.12.2020 obsolet.

Das Repository ist öffentlich und kann unter folgendem Link aufgerufen werden:
https://github.com/aaarl/Spotify-Auswertung-Big-Data-Plattform

Screencast_001: [https://www.youtube.com/watch?v=16CqER2p7Lo](https://www.youtube.com/watch?v=16CqER2p7Lo)

- Gesamter Ablauf/ Roundtrip
    - Minikube starten
    - Start Web App Cluster
    - Start Kafka
    - Start Hadoop
    - Start Spark 
    - Anzeige der entsprechenden Hadoop-Cluster 
    - Erfolgreiches Aufsetzen des Setups)


Screencast_002: [https://www.youtube.com/watch?v=4Zse5u0wUug](https://www.youtube.com/watch?v=4Zse5u0wUug)

- Problematik Upload von Dateien in HDFS (obwohl HDFS erfolgreich konfiguriert und ansprechbar über Web-UI)


Screencast_003: [https://www.youtube.com/watch?v=5oyLurJH1rc](https://www.youtube.com/watch?v=5oyLurJH1rc)

- Kommunikation HDFS über Web-UI
- Aufrufe mit HTTP Request Maker
- Anzeige der entsprechenden Logs für Hadoop
- Anzeige der Datanode Information und des Datanode
- Usage Histogramms
- Veranschaulichung der darin enthaltenen Informationen (Volume Information und Block Pools)
